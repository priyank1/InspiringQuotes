package com.quotesapp.priyank.inspiringquotes;

import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Random;

public class QuotesActivity extends AppCompatActivity {

    String title, quote, author, selectedCategory;
    CoordinatorLayout coordinatorLayout;
    ArrayList<String> myCategories = new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quotes);

        coordinatorLayout = (CoordinatorLayout) findViewById(R.id.coordinatorLayout);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sendEmail(); //Sends random quote via email
            }
        });

        new AsyncGetCategories().execute(); //Fetches categories from the backed server

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_quotes, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.action_refresh:
                new AsyncGetCategories().execute(); //Fetches categories from the backed server
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    //This method will share the randomly selected quote via email to all the recipients.
    protected void sendEmail(){

        String mailto = "mailto:" +
                "?cc=" + "" +
                "&subject=" + Uri.encode(title) +
                "&body=" + Uri.encode(quote+'\n'+'\n'+"- "+author);

        Intent emailIntent = new Intent(Intent.ACTION_SENDTO);
        emailIntent.setData(Uri.parse(mailto));

        try {
            startActivity(emailIntent);
        } catch (ActivityNotFoundException e) {
            //TODO: Handle case where no email app is available
            Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
            sharingIntent.setType("text/plain");
            sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, title);
            sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, quote+'\n'+'\n'+"- "+author);
            startActivity(Intent.createChooser(sharingIntent, getResources().getString(R.string.share_using)));
        }

    }

    //This method will randomly select the quotes category
    protected int randomCategorySelector(int max){

        Random randomNum = new Random();
        int randomInt = 0 + randomNum.nextInt(max);

        return randomInt;

    }

    //This inner class will fetch the categories using the API
    protected class AsyncGetCategories extends AsyncTask<String, String, String>
    {
        ProgressDialog pdLoading = new ProgressDialog(QuotesActivity.this);
        HttpURLConnection conn;
        URL url = null;
        String msgContents, msgCategories;
        int status = 0;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            //this method will be running on UI thread
            pdLoading.setMessage("\tFetching data...");
            pdLoading.setCancelable(false);
            pdLoading.show();

        }
        @Override
        protected String doInBackground(String... params) {

            URL url;
            String jsonStringResult = null;
            StringBuffer response = new StringBuffer();
            try {
                url = new URL("http://quotes.rest/qod/categories.json"); //Categories API
            } catch (MalformedURLException e) {
                throw new IllegalArgumentException("invalid url");
            }

            HttpURLConnection conn = null;
            try {
                conn = (HttpURLConnection) url.openConnection();
                conn.setDoOutput(false);
                conn.setDoInput(true);
                conn.setUseCaches(false);
                conn.setRequestMethod("GET");
                conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded;charset=UTF-8");

                // handle the response
                status = conn.getResponseCode();
                if (status != 200) {
                    throw new IOException("Post failed with error code " + status);
                } else {
                    InputStream  in = new BufferedInputStream(conn.getInputStream());
                    jsonStringResult = inputStreamToString(in);
                    in.close();
                }
            } catch (Exception e) {
                e.printStackTrace();

            } finally {
                if (conn != null) {
                    conn.disconnect();
                }

                return jsonStringResult;
            }


        }

        @Override
        protected void onPostExecute(String result) {

            try{
                JSONObject json_data = new JSONObject(result);

                msgContents = json_data.getString("contents");
                JSONObject json_data_categories = new JSONObject(msgContents);
                msgCategories = json_data_categories.getString("categories");
                JSONObject json_categories = new JSONObject(msgCategories);

                Log.i("CATEGORIES:",msgCategories);

                Iterator<String> iter = json_categories.keys();
                while (iter.hasNext()) {
                    String key = iter.next();
                    try {
                        Object value = json_categories.get(key);
                        Log.i("KEY:",key);
                        myCategories.add(key);
                    } catch (JSONException e) {
                        Log.i("JSONException1:","JSON Error");
                    }
                }

                try{

                    selectedCategory = myCategories.get(randomCategorySelector(myCategories.size()));
                    Log.i("SELECTED CATEGORY:",selectedCategory);
                    Log.i("NUMBER OF CATEGORIES:",myCategories.size()+"");

                    new AsyncGetQuotes().execute(); //Fetches a random quote using the API
                }
                catch (IndexOutOfBoundsException e){

                    Log.i("IndexOutOfBounds","Error");
                    Snackbar.make(coordinatorLayout, "No categories were found", Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();
                }
                pdLoading.dismiss();

            }catch (JSONException e) {
                pdLoading.dismiss();
                Log.i("JSONException2:","JSON Error");
                errorMessage();
            }catch (NullPointerException nullPointer)
            {
                pdLoading.dismiss();
                Log.i("Null","Null Exception");
                errorMessage();
            }

        }

        //Converts input stream from the web server to the String format
        private String inputStreamToString(InputStream is) {
            String rLine;
            StringBuilder response = new StringBuilder();

            InputStreamReader isr = new InputStreamReader(is);

            BufferedReader rd = new BufferedReader(isr);

            try {
                while ((rLine = rd.readLine()) != null) {
                    response.append(rLine);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            return response.toString();
        }

        //This method Shows error message to the user
        private void errorMessage(){

            if(status == 429){
                Snackbar.make(coordinatorLayout, "Too many requests, "+"Error code: "+status, Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
            else if(status == 0){

                Snackbar.make(coordinatorLayout, "Check your internet connection", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
            else{
                Snackbar.make(coordinatorLayout, "Error code: "+status, Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }

            TextView titleTextView = (TextView) findViewById(R.id.title);
            titleTextView.setText("Please, try again later");

            TextView quoteTextView = (TextView) findViewById(R.id.myQuote);
            quoteTextView.setText("");

            TextView authorTextView = (TextView) findViewById(R.id.author);
            authorTextView.setText("");
        }

    }

    //This inner class will fetch the data from the backend server asynchronously
    protected class AsyncGetQuotes extends AsyncTask<String, String, String>
    {
        ProgressDialog pdLoading = new ProgressDialog(QuotesActivity.this);
        HttpURLConnection conn;
        URL url = null;
        String msg;
        int status = 0;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            //this method will be running on UI thread
            pdLoading.setMessage("\tFetching data...");
            pdLoading.setCancelable(false);
            pdLoading.show();

        }
        @Override
        protected String doInBackground(String... params) {

            URL url;
            String jsonStringResult = null;
            StringBuffer response = new StringBuffer();
            try {
                url = new URL("http://quotes.rest/qod.json?category="+selectedCategory);
            } catch (MalformedURLException e) {
                throw new IllegalArgumentException("invalid url");
            }

            HttpURLConnection conn = null;
            try {
                conn = (HttpURLConnection) url.openConnection();
                conn.setDoOutput(false);
                conn.setDoInput(true);
                conn.setUseCaches(false);
                conn.setRequestMethod("GET");
                conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded;charset=UTF-8");

                // handle the response
                status = conn.getResponseCode();
                if (status != 200) {
                    throw new IOException("Post failed with error code " + status);
                } else {
                    InputStream  in = new BufferedInputStream(conn.getInputStream());
                    jsonStringResult = inputStreamToString(in);
                    in.close();
                }
            } catch (Exception e) {
                e.printStackTrace();

            } finally {
                if (conn != null) {
                    conn.disconnect();
                }

                return jsonStringResult;
            }


        }

        @Override
        protected void onPostExecute(String result) {

            try{
                JSONObject json_data = new JSONObject(result);

                msg = json_data.getString("contents");
                JSONObject json_data_categories = new JSONObject(msg);
                JSONArray quotes = json_data_categories.getJSONArray("quotes");


                for (int j = 0; j < quotes.length(); j++) {
                    JSONObject jsonQuotes = quotes.getJSONObject(j);
                    quote = jsonQuotes.getString("quote");
                    author = jsonQuotes.getString("author");
                    title = jsonQuotes.getString("title");

                    Log.i("QUOTE:",quote);
                    Log.i("AUTHOR:",author);
                    Log.i("TITLE:",title);

                }

                pdLoading.dismiss();

                TextView titleTextView = (TextView) findViewById(R.id.title);
                titleTextView.setText(title);

                TextView quoteTextView = (TextView) findViewById(R.id.myQuote);
                quoteTextView.setText(quote);

                TextView authorTextView = (TextView) findViewById(R.id.author);
                authorTextView.setText(author);

                Snackbar.make(coordinatorLayout, "Successfully fetched the quote", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();



            }catch (JSONException e) {
                pdLoading.dismiss();
                Log.i("JSONException3:","JSON Error");
                errorMessage();
            }catch (NullPointerException nullPointer)
            {
                pdLoading.dismiss();
                Log.i("Null","Null Exception");
                errorMessage();
            }

        }

        //Converts input stream from the web server to the String format
        private String inputStreamToString(InputStream is) {
            String rLine;
            StringBuilder response = new StringBuilder();

            InputStreamReader isr = new InputStreamReader(is);

            BufferedReader rd = new BufferedReader(isr);

            try {
                while ((rLine = rd.readLine()) != null) {
                    response.append(rLine);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            return response.toString();
        }

        //Shows error message to the user
        private void errorMessage(){

            if(status == 429){
                Snackbar.make(coordinatorLayout, "Too many requests, "+"Error code: "+status, Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
            else if(status == 0){

                Snackbar.make(coordinatorLayout, "Check your internet connection", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
            else{
                Snackbar.make(coordinatorLayout, "Error code: "+status, Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }

            TextView titleTextView = (TextView) findViewById(R.id.title);
            titleTextView.setText("Please, try again later");

            TextView quoteTextView = (TextView) findViewById(R.id.myQuote);
            quoteTextView.setText("");

            TextView authorTextView = (TextView) findViewById(R.id.author);
            authorTextView.setText("");
        }

    }
}
