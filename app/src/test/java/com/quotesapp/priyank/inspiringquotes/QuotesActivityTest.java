package com.quotesapp.priyank.inspiringquotes;

import android.util.Log;

import org.junit.Test;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import static org.junit.Assert.*;

public class QuotesActivityTest {

    @Test //This method will test the random category selector method
    public void randomCategorySelector() {

        assertTrue(checkIndex());
    }

    public boolean checkIndex(){

        QuotesActivity qa = new QuotesActivity();
        int i = qa.randomCategorySelector(8);

        if(i >= 0 && i <8){

            return true;
        }
        return false;
    }

    @Test //This method will test the connection with the server
    public void testServerConnection() {

       assertTrue(serverResponse());
    }

    //This method will connect with the server and fetch the data
    private boolean serverResponse(){

        URL url;
        boolean connectionStatus = false;
        StringBuffer response = new StringBuffer();
        try {
            url = new URL("http://quotes.rest/qod.json?category=management");
        } catch (MalformedURLException e) {
            throw new IllegalArgumentException("invalid url");
        }

        HttpURLConnection conn = null;
        try {
            conn = (HttpURLConnection) url.openConnection();
            conn.setDoOutput(false);
            conn.setDoInput(true);
            conn.setUseCaches(false);
            conn.setRequestMethod("GET");
            conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded;charset=UTF-8");

            // handle the response
            int status = conn.getResponseCode();
            if (status != 200) {
                connectionStatus = false;
                throw new IOException("Post failed with error code " + status);
            } else {
                BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                String inputLine;
                while ((inputLine = in.readLine()) != null) {
                    response.append(inputLine);
                }
                in.close();

                connectionStatus = true;

            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (conn != null) {
                conn.disconnect();
            }

        }

        return connectionStatus;

    }
}